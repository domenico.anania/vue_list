- Prendere lista di elementi dal JSON;

- Mostrare lista con delle card;

- L'applicazione Vue ha un header con prodotto selezionato;

- Al click su un elemento della lista (button) viene mostrato id del prodotto;

- Utilizzare 2 metodi di approccio:
  
  1 -  ciclare la lista dei prodotti creando le diverse card di conseguenza;
  2 -  creare un nuovo componenente con al suo interno la lista e ciclo il nuovo componente.

- Nel secondo metodo di approccio, utilizziamo lo slot e modifichiamo (ad esempio) header della card;

- Lo slot va alternato sul v-for (ad esempio per indici pari.); 

- Il v-for può essere descritto andando ad inserire due parametri: l'oggetto estratto dalla lista e l'index.